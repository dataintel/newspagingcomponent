import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

@Component({
  selector: 'app-newspaging',
  template: `
    <div class="col-md-6 " style="padding: 10px">
    <div class="col-md-6" style="margin: 0px 0px 5px 0px" *ngFor="let items of pagedItems">
      <div class="row" style="padding: 10px; border: solid 2px rgba(224, 224, 224, 0.33); margin-right: -10px;">
        <h4>{{items.title | slice:0:30}} ...
        <br>  <small style="font-size:12px;">{{items.media_displayName}}, {{items.timestamp}}</small>
        </h4>
        <p class="img-news">
            <img src="{{items.img}}" class="img-responsive"/>
            {{items.text | slice:0:100}} ...
        </p>
      </div>
      
    </div>  
    <div class="col-md-12 ">
        <div class="pull-right">
            <ul *ngIf="pager.pages && pager.pages.length" class="pagination">
            
            <li [ngClass]="{disabled:pager.currentPage === 1}">
                <a (click)="setPage(pager.currentPage - 1)">Prev</a>
            </li>
            
            <li [ngClass]="{disabled:pager.currentPage === pager.totalPages}">
                <a (click)="setPage(pager.currentPage + 1)">Next</a>
            </li>
            
        </ul>
        </div>

    </div>
</div>

 
  `,
  styles: [`
  .img-news img{
    float:left;
    margin-right:10px;
    max-height:80px;
    max-width:130px;
}`]
})

export class BaseWidgetNewsPagingComponent implements OnInit {
  constructor() { }
    public allItems : any[];

    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];
    ngOnInit() {
        this.allItems  = [
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            },
            {
                "title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
                "img": "../../assets/img/noimg.png",
                "timestamp": "15 minutes ago",
                "media_displayName": "Agence France-Presse",
                "text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
            }
        ];
        this.setPage(1);
    }

    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 8) {
            // calculate total pages
            let totalPages = Math.ceil(totalItems / pageSize);

            let startPage: number, endPage: number;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            let startIndex = (currentPage - 1) * pageSize;
            let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            let pages = _.range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.getPager(this.allItems.length, page);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

}
