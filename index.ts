import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetNewsPagingComponent } from './src/base-widget.component';

export * from './src/base-widget.component';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
	BaseWidgetNewsPagingComponent
  ],
  exports: [
    BaseWidgetNewsPagingComponent
  ]
})
export class NewsPagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NewsPagingModule,
      providers: []
    };
  }
}
